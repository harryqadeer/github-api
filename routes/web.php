<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('api.api');
});

Route::get('/test', 'api@index');
//Route::get('/ajaxRequest', 'api@ajaxRequest');

Route::post('/search', 'api@search');

