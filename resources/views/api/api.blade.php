<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<title>GitHub API</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php /*?> <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"><?php */?>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->
<style>
html, body {
	background-color: #fff;
	color: #636b6f;
	font-family: 'Raleway', sans-serif;
	font-weight: 100;
	height: 100vh;
	margin: 0;
}
.full-height {
	height: 100vh;
}
.flex-center {
	align-items: center;
	justify-content: center;
}
.position-ref {
	position: relative;
}
.top-right {
	position: absolute;
	right: 10px;
	top: 18px;
}
.content {
	text-align: center;
}
.title {
	font-size: 84px;
}
.links > a {
	color: #636b6f;
	padding: 0 25px;
	font-size: 12px;
	font-weight: 600;
	letter-spacing: .1rem;
	text-decoration: none;
	text-transform: uppercase;
}
.m-b-md {
	margin-bottom: 30px;
}
.container {
	max-width: 800px;
	margin: 0 auto;
}
</style>
</head>
<body>
<div class="flex-center position-ref full-height">
  <div class="container">
    <div class="content">
      <div class="title m-b-md"> GitHub API </div>
      <div>
        <meta name="_token" content="{{ csrf_token() }}" />
        <meta name="page" content="2" />
        {!! Form::open(['url'=>'','id'=>'search','class'=>''])!!}
        {!! Form::text('keyword', null, ['class' => 'form-control '])!!} 
        {!! Form::submit('Search!',['class'=>'btn btn-success btn-sm form-control'])!!} </div>
      {!! Form::close() !!}
      <div id="meta_data"> </div>
      <div id="search_data">
      
   
      </div>
      <div id="loadMore" style="text-align:center; display:none; margin:50px auto;">
        <button>Load More</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(function(){

$('#search').on('submit',function(e){
    e.preventDefault(e);
	ajaxSearch(1);
    });

$('#loadMore').click(function(e) {
	var page = parseInt($('meta[name="page"]').attr('content'));
   ajaxSearch(page); 
   return false;
});	
	
	function ajaxSearch(page){
		  $.ajaxSetup({
        header:$('meta[name="_token"]').attr('content')
    })
	

	    $.ajax({

        type:"POST",
        url:'/search',
        data:$('#search').serialize()+ "&page=" + page,
        dataType: 'json',
        success: function(data){
           if(data.res == '0'){
				$('#meta_data').html('No Profile Found');
				$('#search_data').html('');
				$('#loadMore').hide();
				}else{
					
				
				
					if(page == 1){
						$('#meta_data').html(data.meta);
						$('#search_data').html('<table border="1" cellpadding="5" style="margin:auto;">'+data.followers+'</table>');
					}else{
						if(data.followers != '0'){
					$('#search_data table').append(data.followers);
						}
					}
					$('#loadMore').show();
					
					if(data.meta == '0'){
					$('#meta_data').html('No Profile Found');
					$('#loadMore').hide();
					}
					if(data.followers == '0'){
					$('#search_data').append('<p>No More Followers Found</p>');
					$('#loadMore').hide();
					}
					
					
			$('meta[name="page"]').attr('content',page+1);
				}
        },
        error: function(data){
			$('#meta_data').html('No Profile Found');
			$('#search_data').html('');
			$('#loadMore').hide();

        }
    })
		}
	
	
});
</script>
</body>
</html>
