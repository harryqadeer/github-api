<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GrahamCampbell\GitHub\Facades\GitHub;
use Illuminate\Support\Facades\View;
use App\Exceptions\Handler;


class api extends Controller
{
    
	
	 public function index()
    {
       
		
		/*if ($request->isMethod('post')){    
            return response()->json(['response' => 'This is post method']); 
        }*/

       // return response()->json(['response' => 'This is get method']);	
        
		$meta = GitHub::user()->show('taylorotwell');
		$followers = GitHub::user()->followers(
		'taylorotwell',
		array(
			'page' => 1,
			'per_page' => 30
			)
		);
		
		$data = array('meta' => $meta, 'followers' => $followers );
		return view('api.test')->with('data',$data);
		
			
    }
	

    public function search()

    {

        $input = request()->all();
		$keyword = $input['keyword'];
		$page = $input['page'];
		if($input){
		$meta = GitHub::user()->show($keyword);
		$followers = GitHub::user()->followers(
		$keyword,
		array(
			'page' => $page,
			'per_page' => 30
			)
		);
		
		if($followers){
		$followers = view('api.loadmore', compact('followers'))->render();
		}else{
			$followers = 0;
			}
		if($meta){
		$meta = view('api.meta', compact('meta'))->render();
		}else{
			$meta = 0;
			}
		
		return response()->json(['res' => '1','followers'=>$followers,'meta' => $meta]);
			}else{
				return response()->json(['res'=>'0']);
			}

    }
	
	
	
	
}
